package game.memory

internal class Tile(val id: String, val symbolId: String) {

    internal var opened: Boolean = false
        private set

    override fun equals(other: Any?): Boolean =
        when (other) {
            is Tile -> id == other.id
            else -> false
        }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    internal fun open() {
        opened = if (opened) {
            throw IllegalStateException("Tile is already opened.")
        } else {
            true
        }
    }

    internal fun close() {
        opened = if (opened) {
            false
        } else {
            throw IllegalStateException("Tile is already closed.")
        }
    }

    internal fun hasSameSymbol(other: Tile): Boolean =
        symbolId == other.symbolId

}