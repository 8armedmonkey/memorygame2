package game.memory

data class TileInfo(
    val id: String,
    val symbolId: String,
    val opened: Boolean
)