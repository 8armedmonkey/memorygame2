package game.memory

internal class OpenedTilesInATurn(private val capacity: Int) {

    internal val allTilesAreWithSimilarSymbol: Boolean
        get() = when {
            isEmpty -> throw IllegalStateException()
            else -> tiles.all { it.hasSameSymbol(tiles[0]) }
        }

    internal val currentNumOfTiles: Int
        get() = tiles.size

    internal val isFull: Boolean
        get() = tiles.size == capacity

    internal val isEmpty: Boolean
        get() = tiles.isEmpty()

    private val tiles: MutableList<Tile> = mutableListOf()

    internal fun open(tile: Tile) {
        if (isFull) {
            throw IllegalStateException(
                String.format("Only %d tiles could be opened in one turn.", capacity)
            )
        }

        if (!isEmpty && !allTilesAreWithSimilarSymbol) {
            throw IllegalStateException(
                "Could not open the tile. Previously opened tiles do not match."
            )
        }

        tile.open()
        tiles.add(tile)
    }

    internal fun clear() {
        tiles.clear()
    }

    internal fun closeAllTiles() {
        tiles.forEach { it.close() }
        clear()
    }

}