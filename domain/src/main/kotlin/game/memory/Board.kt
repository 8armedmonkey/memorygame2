package game.memory

import java.util.*
import kotlin.random.Random

class Board(private val boardSpec: BoardSpec) {

    val numOfTiles: Int

    private val tiles = mutableListOf<Tile>()
    private val openedTilesInATurn = OpenedTilesInATurn(boardSpec.numOfCopies)

    init {
        boardSpec.tileSymbolSet.forEach { symbolId ->
            repeat(boardSpec.numOfCopies) {
                tiles.add(Tile(UUID.randomUUID().toString(), symbolId))
            }
        }

        tiles.shuffle(Random.Default)
        numOfTiles = tiles.size
    }

    fun getTileInfoAt(index: Int): TileInfo =
        synchronized(this) {
            TileInfo(
                tiles[index].id,
                tiles[index].symbolId,
                tiles[index].opened
            )
        }

    fun openTileAt(index: Int): Status {
        synchronized(this) {
            openedTilesInATurn.open(tiles[index])

            return if (openedTilesInATurn.allTilesAreWithSimilarSymbol) {
                if (openedTilesInATurn.isFull) {
                    openedTilesInATurn.clear()
                    Status.MATCH
                } else {
                    Status.LOOKING_FOR_A_MATCH
                }
            } else {
                Status.MISMATCH
            }
        }
    }

    fun closeMismatchTiles() {
        synchronized(this) {
            if (openedTilesInATurn.isEmpty) {
                throw IllegalStateException("No opened tiles.")
            }

            if (openedTilesInATurn.allTilesAreWithSimilarSymbol) {
                throw IllegalStateException("No mismatch opened tiles.")
            }

            openedTilesInATurn.closeAllTiles()
        }
    }

}