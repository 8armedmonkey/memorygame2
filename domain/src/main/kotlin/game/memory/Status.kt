package game.memory

enum class Status {
    LOOKING_FOR_A_MATCH,
    MISMATCH,
    MATCH
}