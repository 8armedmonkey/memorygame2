package game.memory

data class BoardSpec(
    val tileSymbolSet: Set<String>,
    val numOfCopies: Int = 2
) {

    init {
        assertTileSymbolSetNotEmpty()
        assertNumOfCopiesIsNotLtTwo()
    }

    private fun assertTileSymbolSetNotEmpty() {
        if (tileSymbolSet.isEmpty()) {
            throw IllegalArgumentException(
                "Tile symbol set should not be empty."
            )
        }
    }

    private fun assertNumOfCopiesIsNotLtTwo() {
        if (numOfCopies < 2) {
            throw IllegalArgumentException(
                "Number of copies should not be less than 2."
            )
        }
    }


}