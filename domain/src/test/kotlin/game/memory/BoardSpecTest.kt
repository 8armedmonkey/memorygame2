package game.memory

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class BoardSpecTest {

    @Test
    fun whenCreatedWithEmptyTileSetThenItShouldThrowException() {
        assertThrows<IllegalArgumentException> {
            BoardSpec(
                tileSymbolSet = setOf(),
                numOfCopies = 2
            )
        }
    }

    @Test
    fun whenCreatedWithNumberOfCopiesLtTwoThenItShouldThrowException() {
        assertThrows<IllegalArgumentException> {
            BoardSpec(
                tileSymbolSet = setOf("foo"),
                numOfCopies = 1
            )
        }
    }

}