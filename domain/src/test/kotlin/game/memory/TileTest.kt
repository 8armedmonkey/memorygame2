package game.memory

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class TileTest {

    @Test
    fun whenComparedWithTileWithSameIdThenItShouldCompareAsEqual() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("1", "fooSymbol")

        assertEquals(t1, t2)
    }

    @Test
    fun whenComparedWithTileWithDifferentIdThenItShouldCompareAsNotEqual() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("2", "barSymbol")

        assertNotEquals(t1, t2)
    }

    @Test
    fun whenComparedWithTileWithSameIdThenItShouldReturnSameHashCode() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("1", "fooSymbol")

        assertEquals(t1.hashCode(), t2.hashCode())
    }

    @Test
    fun whenComparedWithTileWithDifferentIdThenItShouldReturnDifferentHashCode() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("2", "barSymbol")

        assertNotEquals(t1.hashCode(), t2.hashCode())
    }

    @Test
    fun whenOpenedThenItsOpenedStateShouldBeTrue() {
        val t = Tile("1", "fooSymbol")
        t.open()

        assertTrue(t.opened)
    }

    @Test
    fun whenClosedThenItsOpenedStateShouldBeFalse() {
        val t = Tile("1", "fooSymbol")
        t.open()
        t.close()

        assertFalse(t.opened)
    }

    @Test
    fun whenComparedWithTileWithSameSymbolIdThenItShouldReturnTrue() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("2", "fooSymbol")

        assertTrue(t1.hasSameSymbol(t2))
    }

    @Test
    fun whenComparedWithTileWithDifferentSymbolIdThenItShouldReturnFalse() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("2", "barSymbol")

        assertFalse(t1.hasSameSymbol(t2))
    }

}