package game.memory

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class OpenedTilesInATurnTest {

    private lateinit var openedTilesInATurn: OpenedTilesInATurn

    @BeforeEach
    fun setUp() {
        openedTilesInATurn = OpenedTilesInATurn(3)
    }

    @Test
    fun whenOpenTilesAndMaxCapacityHasNotBeenReachedThenItShouldNotThrowException() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))
        openedTilesInATurn.open(Tile("3", "fooSymbol"))

        assertEquals(3, openedTilesInATurn.currentNumOfTiles)
    }

    @Test
    fun whenOpenTilesAndMaxCapacityHasBeenReachedThenItShouldThrowException() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))
        openedTilesInATurn.open(Tile("3", "fooSymbol"))

        assertThrows<IllegalStateException> {
            openedTilesInATurn.open(Tile("4", "fooSymbol"))
        }
    }

    @Test
    fun whenOpenTilesThenItShouldUpdateCurrentNumOfTiles() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))

        assertEquals(2, openedTilesInATurn.currentNumOfTiles)
    }

    @Test
    fun whenOpenTilesAndNotAllTilesAreOfSimilarSymbolThenItShouldThrowException() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "barSymbol"))

        assertThrows<IllegalStateException> {
            openedTilesInATurn.open(Tile("3", "bazSymbol"))
        }
    }

    @Test
    fun whenClearedThenItShouldRemoveAllAddedTiles() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))
        openedTilesInATurn.clear()

        assertEquals(0, openedTilesInATurn.currentNumOfTiles)
    }

    @Test
    fun whenCloseAllTilesThenItShouldCloseAndRemoveAllAddedTiles() {
        val t1 = Tile("1", "fooSymbol")
        val t2 = Tile("2", "barSymbol")

        openedTilesInATurn.open(t1)
        openedTilesInATurn.open(t2)
        openedTilesInATurn.closeAllTiles()
    }

    @Test
    fun whenMaxCapacityHasBeenReachedThenItShouldBeFull() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))
        openedTilesInATurn.open(Tile("3", "fooSymbol"))

        assertTrue(openedTilesInATurn.isFull)
    }

    @Test
    fun whenMaxCapacityHasNotBeenReachedThenItShouldNotBeFull() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))

        assertFalse(openedTilesInATurn.isFull)
    }

    @Test
    fun whenContainsNoTilesThenItShouldBeEmpty() {
        openedTilesInATurn.clear()

        assertTrue(openedTilesInATurn.isEmpty)
    }

    @Test
    fun whenContainsAtLeastOneTileThenItShouldNotBeEmpty() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))

        assertFalse(openedTilesInATurn.isEmpty)
    }

    @Test
    fun whenAllTilesAreOfSimilarSymbolThenItShouldIndicateThat() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))
        openedTilesInATurn.open(Tile("3", "fooSymbol"))

        assertTrue(openedTilesInATurn.allTilesAreWithSimilarSymbol)
    }

    @Test
    fun whenAllTilesAreNotOfSimilarSymbolThenItShouldIndicateThat() {
        openedTilesInATurn.open(Tile("1", "fooSymbol"))
        openedTilesInATurn.open(Tile("2", "fooSymbol"))
        openedTilesInATurn.open(Tile("3", "barSymbol"))

        assertFalse(openedTilesInATurn.allTilesAreWithSimilarSymbol)
    }

    @Test
    fun whenEmptyAndQueriedForAllTilesAreOfSimilarSymbolThenItShouldThrowException() {
        openedTilesInATurn.clear()

        assertThrows<IllegalStateException> {
            openedTilesInATurn.allTilesAreWithSimilarSymbol
        }
    }

}