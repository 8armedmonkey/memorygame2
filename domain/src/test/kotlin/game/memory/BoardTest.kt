package game.memory

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class BoardTest {

    private lateinit var board: Board
    private lateinit var boardTestHelper: BoardTestHelper

    @BeforeEach
    fun setUp() {
        board = Board(
            BoardSpec(
                tileSymbolSet = setOf("foo", "bar", "baz"),
                numOfCopies = 3
            )
        )

        boardTestHelper = BoardTestHelper(board)
    }

    @Test
    fun whenCreatedThenItShouldContainCorrectNumberOfTiles() {
        // numOfTiles = tileSymbolSet.size() * numOfCopies
        assertEquals(9, board.numOfTiles)
    }

    @Test
    fun whenOpenFirstTileInOneTurnThenTheStatusShouldBeLookingForAMatch() {
        val status = boardTestHelper.openNextTileWithSymbolId("foo")

        assertEquals(Status.LOOKING_FOR_A_MATCH, status)
    }

    @Test
    fun whenOpenNextTilesInOneTurnAndItMatchesPrevOpenedTilesThenTheStatusShouldBeLookingForAMatch() {
        boardTestHelper.openNextTileWithSymbolId("foo")

        val status = boardTestHelper.openNextTileWithSymbolId("foo")

        assertEquals(Status.LOOKING_FOR_A_MATCH, status)
    }

    @Test
    fun whenOpenLastTilesInOneTurnAndItMatchesPrevOpenedTilesThenTheStatusShouldBeMatch() {
        boardTestHelper.openNextTileWithSymbolId("foo")
        boardTestHelper.openNextTileWithSymbolId("foo")

        val status = boardTestHelper.openNextTileWithSymbolId("foo")

        assertEquals(Status.MATCH, status)
    }

    @Test
    fun whenOpenNextTileInOneTurnAndItIsDifferentFromPrevOpenedTilesThenTheOpenedTilesShouldBeMismatch() {
        boardTestHelper.openNextTileWithSymbolId("foo")

        val status = boardTestHelper.openNextTileWithSymbolId("bar")

        assertEquals(Status.MISMATCH, status)
    }

    @Test
    fun whenOpenTileAndItIsAlreadyOpenedThenItShouldThrowException() {
        board.openTileAt(0)

        assertThrows<IllegalStateException> {
            board.openTileAt(0)
        }
    }

    @Test
    fun whenCloseMismatchTilesThenAllMismatchTilesShouldBeClosed() {
        val fooIndex = boardTestHelper.getNextTileIndexWithSymbolId("foo")
        val barIndex = boardTestHelper.getNextTileIndexWithSymbolId("bar")

        board.openTileAt(fooIndex)
        board.openTileAt(barIndex)
        board.closeMismatchTiles()

        assertFalse(board.getTileInfoAt(fooIndex).opened)
        assertFalse(board.getTileInfoAt(barIndex).opened)
    }

    @Test
    fun whenCloseMismatchTilesAndNoOpenedTilesThenItShouldThrowException() {
        assertThrows<IllegalStateException> {
            board.closeMismatchTiles()
        }
    }

    @Test
    fun whenCloseMismatchTilesAndAllOpenedTilesAreMatchingThenItShouldThrowException() {
        boardTestHelper.openNextTileWithSymbolId("foo")
        boardTestHelper.openNextTileWithSymbolId("foo")

        assertThrows<IllegalStateException> {
            board.closeMismatchTiles()
        }
    }

}