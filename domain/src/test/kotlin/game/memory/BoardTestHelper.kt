package game.memory

internal class BoardTestHelper(
    private val board: Board
) {

    private val lookup: MutableMap<String, MutableList<Int>> = mutableMapOf()

    init {
        (0 until board.numOfTiles).forEach { index ->
            val symbolId = board.getTileInfoAt(index).symbolId
            val indices = lookup[symbolId] ?: mutableListOf()

            indices.add(index)
            lookup[symbolId] = indices
        }
    }

    internal fun getNextTileIndexWithSymbolId(symbolId: String): Int {
        val indices = lookup[symbolId] ?: throw IllegalArgumentException()

        return indices.find { index -> !board.getTileInfoAt(index).opened }
            ?: throw IllegalStateException()
    }

    internal fun openNextTileWithSymbolId(symbolId: String): Status {
        val unopenedTileIndex = getNextTileIndexWithSymbolId(symbolId)

        return board.openTileAt(unopenedTileIndex)
    }

}